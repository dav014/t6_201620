package taller.estructuras;


public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar
	public enum tiposcoli
	{
		Serios,
		Normales
		
	}
	
	protected tiposcoli tipoc;
	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private NodoHash[] tabla;
	
	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		tabla = new NodoHash[2000000];
		for (int i = 0 ; i< 2000000; i++)
		{
			tabla[i] = null;
		}
	
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		this.capacidad = capacidad;
		this.factorCargaMax = factorCargaMax;
		tabla = new NodoHash[capacidad];
		for (int i = 0 ; i< capacidad; i++)
		{
			tabla[i] = null;
		}
	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		int hash = (int) ((int) llave % factorCargaMax);
		while (tabla[hash] != null && tabla[hash].getLlave() != llave)
		{
			hash = ((hash +1 ));
		}
		tabla[hash] = new NodoHash<K, V>(llave, valor);
		
	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones

		int hash = (int)((int) llave % factorCargaMax);
		while (tabla[hash] != null && tabla[hash].getLlave() != llave)
		{
			hash = ((hash +1 ));
		}
		if (tabla[hash] == null)
				{
			return null;
	
				}
		else{
			return (V) tabla[hash].getValor();
			
		}
		}
	

	public V delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones

		int hash = (int)((int) llave % factorCargaMax);
		while (tabla[hash] != null && tabla[hash].getLlave() != llave)
		{
			hash = ((hash +1 ));
		}
		if (tabla[hash] == null)
				{
			return null;
	
				}
		else{
			tabla[hash] = null;
			return (V) tabla[hash].getValor();
			
		}
		
		
	}

	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		return 0;
		
		
		
	}
	
		//TODO: Permita que la tabla sea dinamica

}